$al = '0123456789'
		
('a'..'z').each do |s|
	$al += s
end
		
('A'..'Z').each do |s|
	$al += s
end



class Integer

	def to_sx
		num = self
		return $al[self] if num < $al.size
		r = ''
		while num != 0
			num, i = num / $al.size, num % $al.size
			r = $al[i] + r
		end
		r
	end

end


class String

	def to_ix
	    r = 0
	    self.reverse.chars.each_with_index do |c,i|
	    	r += ($al.size**i) * ($al.index(c))  
	    end
		r
	end

end







